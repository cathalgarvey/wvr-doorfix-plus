## Background

There is a bug in Starbound that can lead to some doors on ships becoming corrupted so that they become impassible,
even if the actual 'door' has been removed.

The only commonplace fix for this bug has been [Weaver's 'Ghost Hatch/Door Remover' mod][hatchfix],
which appears to resolve the issue in most cases..provided you can place the faux 'door' required
to 'mine' out the erroneous blocks.

Unfortunately, the ship teleporter in some ships is directly left of one of the first ship doors,
preventing the fix from being applied. This is also the first doorway in the ship and remains
unaltered as the ship grows, so it is probably the most commonly affected door.

I was affected by this issue, so I've added a left-facing version of the fix.
The usual and original caveats apply - this can cause problems for you that it
provides no fix for, so be very careful how you apply the fix. I suggest visiting
the [original mod's homepage][hatchfix] and reading there and reading the comments.

It is worth pointing out that I have never written a Starbound mod and I hacked this extension
together with copy/paste and some crude intuitions. It worked for me, at least. No warranty implied.

Weaver: thanks. :)

[hatchfix]: https://steamcommunity.com/sharedfiles/filedetails/?id=755391531

## Installation

1. Put your character in your ship, then shut down Starbound.
1. Backup your `storage` folder.
1. Copy the file `hatchfix_withleft.pak` to your `mods` folder.
1. Start your character _locally_: as you are in your ship, this should not affect multiplayer, I think.
1. Apply the fix with care: you can craft the hackish faux doors from the `c` menu, and then try to place the doors so they overlap by one tile with the broken 'door'. Don't interact with the door: just place it, then mine it out immediately.
1. Repeat this operation a second time for the second layer of the broken door, to erase that also. Mine the faux 'door' out.
1. Put the faux door in the waste-bin in your Inventory.
1. Shut down starbound.
1. Delete the mod from your `mods` folder.
1. Continue playing, hopefully?
